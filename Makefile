VERSION="1.0"
UNAME=$(shell uname)
SDL=$(shell sdl2-config --cflags --libs)

CC=zig cc
FLAGS=-std=c99 -lm -Wall -Wextra -Wpedantic -Os -Wno-strict-prototypes
#ifeq ($(UNAME),Linux)
#	# _BSD_SOURCE is required by older versions of GCC to find various posix extensions like realpath().
#	# _DEFAULT_SOURCE is the same deal, except newer versions of GCC need it
#	# -lm is required for math.h
#	FLAGS=-std=c99 -D _BSD_SOURCE -D _DEFAULT_SOURCE -lm -Wall -Wextra
#	# -Wno-misleading-indentation silences warnings which are entirely spurious.
#	# -Wno-format-truncation likewise silences spurious warnings regarding snprintf() truncation.
#	FLAGS:=$(FLAGS) -Wno-misleading-indentation
#endif
#ifeq ($(UNAME),Darwin)
#	# FLAGS:=$(FLAGS) -fsanitize=undefined
#	# FLAGS:=$(FLAGS) -fsanitize=address
#endif

EMBEDED_FILES=c/resources.o

ALL_DOCS=docs/lil.html docs/lilt.html docs/decker.html docs/format.html

.PHONY: all clean install uninstall test docs
all: lilt decker docs

docs: $(ALL_DOCS)

$(ALL_DOCS): %.html: %.md
	pandoc -s -f gfm --metadata title="${basename $(<F)}" $< > $@

$(EMBEDED_FILES): c/resources.zig
	zig build-obj -femit-bin=$(EMBEDED_FILES) --main-pkg-path . c/resources.zig

lilt: $(EMBEDED_FILES) c/lilt.c c/*.h
	$(CC) ./c/lilt.c $(EMBEDED_FILES) -o ./lilt $(FLAGS) -DVERSION="\"$(VERSION)\""

decker: $(EMBEDED_FILES) c/decker.c c/*.h
	$(CC) ./c/decker.c $(EMBEDED_FILES) -o ./decker $(SDL) -lSDL2_image $(FLAGS) -DVERSION="\"$(VERSION)\""

clean:
	rm -f $(ALL_DOCS)
	rm -f ./lilt ./decker $(EMBEDED_FILES)*
	rm -rf ./js/build/
	rm -f docs/*.html

install:
	./scripts/install.sh

uninstall:
	./scripts/uninstall.sh

test: lilt
	./scripts/test_interpreter.sh "./lilt "
	./lilt tests/dom/domtests.lil
	./lilt tests/dom/test_roundtrip.lil

.PHONY: js jsres testjs web-decker
js: jsres
	mkdir -p js/build/
	echo "VERSION=\"${VERSION}\"\n" > js/build/lilt.js
	cat js/lil.js js/repl.js >> js/build/lilt.js

testjs: js
	./scripts/test_interpreter.sh "node js/build/lilt.js"
	node js/build/lilt.js tests/dom/domtests.lil
	node js/build/lilt.js tests/dom/test_roundtrip.lil

web-decker: js
	./scripts/web_decker.sh examples/decks/tour.deck js/build/decker.html $(VERSION)
	open js/build/decker.html
