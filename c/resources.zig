const _js_lil_js = @embedFile("../js/lil.js");
export const js_lil_js = _js_lil_js;
export const js_lil_js_len = _js_lil_js.len;

const _js_decker_js = @embedFile("../js/decker.js");
export const js_decker_js = _js_decker_js;
export const js_decker_js_len = _js_decker_js.len;

const _js_decker_html = @embedFile("../js/decker.html");
export const js_decker_html = _js_decker_html;
export const js_decker_html_len = _js_decker_html.len;

const _examples_decks_tour_deck = @embedFile("../examples/decks/tour.deck");
export const examples_decks_tour_deck = _examples_decks_tour_deck;
export const examples_decks_tour_deck_len = _examples_decks_tour_deck.len;
